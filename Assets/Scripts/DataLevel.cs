using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Data
{
    public string namePrefabs;
    public Vector3 position;
    public Vector3 rotation;

    public Data(string namePrefabs, Vector3 prefabsValue, Vector3 rotation)
    {
        this.namePrefabs = namePrefabs;
        this.position = prefabsValue;
        this.rotation = rotation;
    }
}

[System.Serializable]
public class DataLevel : ScriptableObject
{
    [SerializeField] private List<Data> dataList = new List<Data>();


    public List<Data> data { get => dataList; set => dataList = value; }
}
