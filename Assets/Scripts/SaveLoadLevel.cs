using System.Collections.Generic;
using UnityEditor;
using UnityEngine;



public static class SaveLoadLevel
{

    public static List<string> level = new List<string>();

    [System.Obsolete]
    public static DataLevel SaveData(string nameLevel)
    {
        DataLevel data = ScriptableObject.CreateInstance<DataLevel>();
        AssetDatabase.CreateAsset(data, "Assets/Resources/" + nameLevel + ".asset");

        GameObject[] prefabs = GameObject.FindObjectsOfType<GameObject>();

        for (int i = 0; i < prefabs.Length; i++)
        {
            if (EditorUtility.GetPrefabType(prefabs[i]) == PrefabType.PrefabInstance)
            {
                var tmp = EditorUtility.FindPrefabRoot(prefabs[i]);

                if (tmp == prefabs[i])
                {
                    var matrix = Matrix4x4.TRS(tmp.transform.position, tmp.transform.rotation, Vector3.one);
                    data.data.Add(new Data(tmp.name, matrix.GetPosition(), tmp.transform.localRotation.eulerAngles));
                }
            }
        }

        return data;

    }

    public static void RefreshPOP()
    {
        var folder = AssetDatabase.FindAssets("t:ScriptableObject", new[] { "Assets/Resources" });

        level.Clear();

        foreach (var item in folder)
        {
            var path = AssetDatabase.GUIDToAssetPath(item);
            var levelData = AssetDatabase.LoadAssetAtPath<DataLevel>(path);
            level.Add(levelData.name);
        }
    }

    public static void LoadData(string levelToLoad)
    {
        var myData = Resources.Load<DataLevel>(levelToLoad);
        var defaulPrefab = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs" });

        var parentGo = new GameObject();
        parentGo.name = levelToLoad;
        parentGo.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);

        for (int i = 0; i < myData.data.Count; i++)
        {
            foreach (var def in defaulPrefab)
            {
                var pathDefault = AssetDatabase.GUIDToAssetPath(def);
                var goDef = AssetDatabase.LoadAssetAtPath<GameObject>(pathDefault);

                if (goDef.name.Equals(myData.data[i].namePrefabs))
                {
                    GameObject spawn = (GameObject)PrefabUtility.InstantiatePrefab(goDef);
                    Undo.RegisterCreatedObjectUndo(spawn, spawn.name.ToString());
                    spawn.transform.SetPositionAndRotation(myData.data[i].position, Quaternion.Euler(myData.data[i].rotation));
                    spawn.transform.parent = parentGo.transform;

                }
            }
        }
    }
}


