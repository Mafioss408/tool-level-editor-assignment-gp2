using log4net.Core;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


public class LevelCustomEditor : EditorWindow
{
    #region VARIABLE
    public float distanceSnap;
    public float distanceResetSnap;
    public string nameMap;



    protected GameObject[] prefabs;
    protected GameObject selectPrefabs;

    protected Transform cam;

    private int indexPoP = 0;

    #endregion

    #region SO
    SerializedObject so;
    SerializedProperty distanceSnapP;
    SerializedProperty distanceResetSnapP;
    SerializedProperty nameMapP;
    #endregion


    [MenuItem("Tools/LevelEditor")]
    public static void OpenWindows()
    {
        WindowViewport win = GetWindow<WindowViewport>();
        win.titleContent = new GUIContent("Level Editor");
    }

    private void Awake()
    {
        distanceSnap = EditorPrefs.GetFloat(string.Format("distance", distanceSnap), 2);
        distanceResetSnap = EditorPrefs.GetFloat(string.Format("distanceReset", distanceResetSnap), 10);
    }

    private void OnEnable()
    {
        SceneView.duringSceneGui += DuringSceneGUI;
        so = new SerializedObject(this);
        FindPrefabsFolder();
        InitProperty();
        SaveLoadLevel.RefreshPOP();
    }

    protected virtual void OnDisable()
    {
        EditorPrefs.SetFloat(string.Format("distance", distanceSnap), distanceSnap);
        EditorPrefs.SetFloat(string.Format("distanceReset", distanceResetSnap), distanceResetSnap);
        SceneView.duringSceneGui -= DuringSceneGUI;
    }

    private void OnProjectChange()
    {
        SaveLoadLevel.RefreshPOP();
    }

    private void OnGUI()
    {
        so.Update();

        GUI.TextField(new Rect(20, 20, 200, 20), "VARIABLE:", formatText());

        GUILayout.BeginArea(new Rect(20, 50, 400, 60));

        GUILayout.BeginHorizontal();
        EditorGUILayout.TextField("distanceSnap", formatText());
        distanceSnap = EditorGUILayout.Slider(distanceSnap, 1, 10);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.TextField("distanceResetSanp", formatText());
        distanceResetSnap = EditorGUILayout.Slider(distanceResetSnap, 10, 20);
        GUILayout.EndHorizontal();

        GUILayout.EndArea();


        GUILayout.BeginArea(new Rect(20, 100, 372, 100));

        GUILayout.Space(8);
        GUILayout.TextField("SAVE - LOAD:", formatText());
        GUILayout.Space(8);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Save Level"))
        {
            if (!nameMap.Equals(string.Empty))
            {
                DataLevel data = SaveLoadLevel.SaveData(nameMap);
                EditorUtility.SetDirty(data);
                SaveLoadLevel.RefreshPOP();
            }
        }
        nameMap = EditorGUILayout.TextField(nameMap);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Load Level") && SaveLoadLevel.level.Count > 0)
            SaveLoadLevel.LoadData(SaveLoadLevel.level[indexPoP]);

        indexPoP = EditorGUILayout.Popup(indexPoP, SaveLoadLevel.level.ToArray());

        GUILayout.EndHorizontal();
        GUILayout.EndArea();


        if (so.ApplyModifiedProperties())
            SceneView.RepaintAll();

    }

    protected virtual void DuringSceneGUI(SceneView view)
    {
        cam = view.camera.transform;
    }

    void FindPrefabsFolder()
    {
        string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs" });
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        prefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();
        selectPrefabs = null;
    }

    void InitProperty()
    {
        distanceSnapP = so.FindProperty("distanceSnap");
        distanceResetSnapP = so.FindProperty("distanceResetSnap");
        nameMapP = so.FindProperty("nameMap");
    }

    protected void ButtonGUI(float posY)
    {
        GUI.TextField(new Rect(20, posY + 10, 200, 20), "UNDO:", formatText());

        if (GUI.Button(new Rect(20, posY + 30, 120, 20), "Undo Prefabs"))
        {
            Undo.PerformUndo();
        }
    }

    protected GUIStyle formatText()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        return style;
    }

}



