﻿using System.Collections;
using Unity.EditorCoroutines.Editor;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;


public class SpawnPrefabs : LevelCustomEditor
{

    private bool IsSnap = false;
    private Matrix4x4 posSpawn;

    protected override void DuringSceneGUI(SceneView view)
    {
        base.DuringSceneGUI(view);
        PositionPrefabs();

    }

    void PositionPrefabs()
    {

        if (selectPrefabs == null) return;

        if (HandleUtility.PlaceObject(Event.current.mousePosition, out Vector3 pos, out Vector3 posNormal))
        {
            if (!IsSnap)
            {
                selectPrefabs.transform.rotation = RotatePreview(selectPrefabs);
                DrawPrefabsPreviews(pos);

                if (!GroundFree(selectPrefabs, pos, 2.35f))
                {
                    SnapPrefabs(selectPrefabs, pos);
                    SpawnInput(pos, selectPrefabs.transform.rotation);
                }
            }
            else if (IsSnap)
            {
                ResetSnap(posSpawn.GetPosition(), pos);
                DrawPrefabsPreviews(posSpawn.GetPosition());

                if (!GroundFree(selectPrefabs, pos, 2f))
                    SpawnInput(posSpawn.GetPosition(), posSpawn.rotation);
            }
        }
    }

    void SpawnInput(Vector3 position, Quaternion rotation)
    {
        if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyDown)
        {
            TrySpawnObjects(position, rotation);
        }
    }

    void DrawPrefabsPreviews(Vector3 positionCurrent)
    {
        MeshFilter[] filters = selectPrefabs.GetComponentsInChildren<MeshFilter>();

        foreach (MeshFilter filter in filters)
        {

            Matrix4x4 childToPose = filter.transform.localToWorldMatrix;
            Matrix4x4 childToWorldMtx = Matrix4x4.TRS(childToPose.GetPosition() + positionCurrent, selectPrefabs.transform.localRotation * filter.transform.localRotation, filter.transform.localScale);

            Mesh mesh = filter.sharedMesh;
            Material mat = filter.GetComponent<MeshRenderer>().sharedMaterial;
            mat.SetPass(0);
            Graphics.DrawMeshNow(mesh, childToWorldMtx);
        }
    }

    Quaternion RotatePreview(GameObject obj)
    {
        if (Event.current.keyCode == KeyCode.R && Event.current.type == EventType.KeyDown)
            return obj.transform.rotation * Quaternion.Euler(0, 90, 0);
        else
            return obj.transform.rotation;
    }

    void TrySpawnObjects(Vector3 prefabsPosition, Quaternion prefabsRotation)
    {
        GameObject prefab = (GameObject)PrefabUtility.InstantiatePrefab(selectPrefabs);
        Undo.RegisterCreatedObjectUndo(prefab, prefab.name.ToString());
        prefab.transform.SetPositionAndRotation(prefabsPosition, prefabsRotation);
    }

    void SnapPrefabs(GameObject prefabsPreview, Vector3 positionCurrent)
    {

        Door[] prefabsDoor = prefabsPreview.GetComponentsInChildren<Door>();

        for (int i = 0; i < prefabsDoor.Length; i++)
        {

            Handles.DrawWireDisc(prefabsDoor[i].transform.position + positionCurrent + prefabsPreview.transform.position, Vector3.up, distanceSnap);

            if (Physics.Raycast(prefabsDoor[i].transform.position + positionCurrent, prefabsDoor[i].transform.right, out RaycastHit hit))
            {
                if (hit.collider.TryGetComponent(out Door door))
                {
                    float direction = Vector3.Dot(prefabsDoor[i].transform.right, door.transform.right);
                    float distanceDoor = Vector3.Distance(prefabsDoor[i].transform.position + positionCurrent, door.transform.position);

                    GameObject prefabHit = EditorUtility.FindPrefabRoot(door.gameObject);


                    if (direction < 0 && distanceDoor < distanceSnap)
                    {
                        var snapPosition = prefabHit.transform.position - (new Vector3(prefabsDoor[i].transform.position.x * 2, 0f, prefabsDoor[i].transform.position.z * 2));

                        posSpawn = Matrix4x4.TRS(snapPosition, prefabsPreview.transform.rotation, Vector3.one);
                        IsSnap = true;
                    }
                }
            }
        }
    }

    bool GroundFree(GameObject prefabs, Vector3 position, float radius)
    {
        if (Physics.CheckSphere(prefabs.transform.position + position, radius))
        {
            return true;
        }
        return false;
    }

    void ResetSnap(Vector3 currentSnapPos, Vector3 rayPos)
    {
        if (Vector3.Distance(rayPos, currentSnapPos) > distanceResetSnap)
            IsSnap = false;
    }
}