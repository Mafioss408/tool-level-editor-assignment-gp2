using UnityEngine;
using UnityEditor;

public class WindowViewport : SpawnPrefabs
{
    private Rect window = new Rect(8, 8, 200, 200);

    protected override void DuringSceneGUI(SceneView view)
    {
        base.DuringSceneGUI(view);


        BeginWindows();
        view.Repaint();


        window = GUILayout.Window(0, window, WindowView, "LEVEL EDITOR", GUILayout.MinWidth(100),
                                                                         GUILayout.MaxWidth(250),
                                                                         GUILayout.MinHeight(450),
                                                                         GUILayout.MaxHeight(500));

        EndWindows();
    }

    void WindowView(int windowID)
    {

        GUILayout.ExpandWidth(true);
        GUILayout.ExpandHeight(true);
        GUILayout.FlexibleSpace();

        ScaleWindow();
        DrawUtilityWindow();
    }

    void ScaleWindow()
    {

        if (Event.current.mousePosition.x < 3 || Event.current.mousePosition.x > window.width - 5)
        {
            EditorGUIUtility.AddCursorRect(new Rect(0, 0, 5000, 5000), MouseCursor.ResizeHorizontal);

            if (Event.current.type == EventType.MouseDrag && Event.current.button == 0)
            {
                window = new Rect(window.x, window.y, window.width + Event.current.delta.x, window.height);
                Event.current.Use();
            }
        }
        if (Event.current.mousePosition.y < 3 || Event.current.mousePosition.y > window.height - 5)
        {
            EditorGUIUtility.AddCursorRect(new Rect(0, 0, 5000, 5000), MouseCursor.ResizeVertical);

            if (Event.current.type == EventType.MouseDrag && Event.current.button == 0)
            {
                window = new Rect(window.x, window.y, window.width, window.height + Event.current.delta.y);
                Event.current.Use();
            }
        }


        if (Event.current.isMouse && Event.current.button == 1)
        {
            GUI.DragWindow();
            Event.current.Use();
        }
    }

    void DrawUtilityWindow()
    {
        Handles.BeginGUI();

        Rect positionIcon = new Rect(20, 35, 60, 60);
        Rect positionText = new Rect(85, 55, 110, 20);

        for (int i = 0; i < prefabs.Length; i++)
        {
            GameObject obj = prefabs[i];
            Texture icon = AssetPreview.GetAssetPreview(obj);


            EditorGUI.BeginChangeCheck();

            if (window.width > 200)
            {
                GUIStyle style = new GUIStyle();
                style.normal.textColor = Color.white;
                GUI.TextField(positionText, prefabs[i].name.ToString(), style);

            }

            if (GUI.Button(positionIcon, icon))
            {
                if (obj == selectPrefabs)
                    selectPrefabs = null;
                else
                    selectPrefabs = obj;
            }

            EditorGUI.EndChangeCheck();


            positionIcon.y += positionIcon.height + 5;
            positionText.y += positionText.height + 45;
        }

        if (window.width > 150 && window.height > 480)
        {
            ButtonGUI(positionIcon.y);
        }

        Handles.EndGUI();
    }
}

