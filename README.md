# Tool Level Editor Assignment GP2

## WindowEditor - window sceneView

- Nella sceneView è possibile selezionare il prefab, estendendo la window si vedranno i nomi dei prefabs e il comando Undo.
- Undo  o Shortcut (da tastiera) = annulla l'ultimo comando.

## WindowEditor - custom inspector

Nel custom inspector sono presenti  2 variabili per lo snap:

- Distance Snap = La distanza minima prima che avvenga lo snap della preview.
- Distance Reset Snap = Una volta effettuato uno snap, la distanza di reset indica a quale distanza lo snap verrà annullato.

Sono anche presenti i due bottoni per effettuare il salvataggio del livello

- Save = Inserire il nome del file, nel field, il file salvato finirà nella cartella "Resources"
- Load = Con la tendina accanto è possibile selezionare il livello che si vuole ricaricare, verrà creato un GameObject vuoto come root per il livello che si genererà.

## Comandi

- Space = Instanzia il prefab, sia snappato o meno.
- R = Ruota la preview del prefab selezionato, solo se non è snappato.
- Mouse tasto dx = Sposta la window nella sceneView.
- Mouse tasto sx = Ridimensiona la window ( solo quando il cursore si trova nei bordi).


## Per consegna finale
- Non essendo riuscito ad implementarli per questa consegna, andrò ad implementare gli handles che permetteranno di muovere i livelli e i vari prefabs una volta spawnati.
